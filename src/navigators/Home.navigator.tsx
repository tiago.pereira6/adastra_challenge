import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {CoinList, SingleCoin} from '../screens';
import {Navigation} from '../models';

const Stack = createStackNavigator<Navigation>();
const HomeNavigator: React.FC = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {elevation: 0},
      }}>
      <Stack.Screen
        name="CoinList"
        component={CoinList}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="SingleCoin"
        component={SingleCoin}
        options={{
          headerTitle: '',
          headerBackTitle: '',
        }}
      />
    </Stack.Navigator>
  );
};

export default HomeNavigator;
