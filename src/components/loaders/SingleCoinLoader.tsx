import React from 'react';
import {View, StyleSheet, Dimensions} from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

// Dimensions
const {width, height} = Dimensions.get('screen');

const SingleCoinLoader: React.FC = ({}) => {
  return (
    <SkeletonPlaceholder>
      <View style={{height: height}}>
        <View style={styles.skeletonItem} />
      </View>
    </SkeletonPlaceholder>
  );
};

const styles = StyleSheet.create({
  skeletonItem: {
    width: width * 0.8,
    borderRadius: 35,
    padding: 20,
    alignItems: 'center',
    height: height * 0.5,
  },
});

export default SingleCoinLoader;
