import React from 'react';
import {View, StyleSheet, Dimensions} from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

// Dimensions
const {width, height} = Dimensions.get('screen');

const CoinListLoader: React.FC = ({}) => {
  return (
    <SkeletonPlaceholder>
      <View style={{height: height}}>
        <View style={styles.skeletonItem} />
        <View style={styles.skeletonItem} />
        <View style={styles.skeletonItem} />
        <View style={styles.skeletonItem} />
        <View style={styles.skeletonItem} />
        <View style={styles.skeletonItem} />
        <View style={styles.skeletonItem} />
        <View style={styles.skeletonItem} />
        <View style={styles.skeletonItem} />
      </View>
    </SkeletonPlaceholder>
  );
};

const styles = StyleSheet.create({
  skeletonItem: {
    margin: 5,
    width: width * 0.9,
    height: 60,
    borderRadius: 20,
    alignSelf: 'center',
  },
});

export default CoinListLoader;
