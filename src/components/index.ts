// Home
import CoinCard from './home/CoinCard';
import SearchInput from './home/SearchInput';

// Single Coin
import SingleCoinCard from './singleCoin/SingleCoinCard';

// Loaders
import CoinListLoader from './loaders/ConlistLoader';
import SingleCoinLoader from './loaders/SingleCoinLoader';

// Error
import Error from './error/Error';

export {
  CoinCard,
  SearchInput,
  SingleCoinCard,
  CoinListLoader,
  SingleCoinLoader,
  Error,
};
