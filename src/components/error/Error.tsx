import React, {useState, useEffect, useCallback} from 'react';
import {View, Text, Modal, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

// Theme
import {themeColors} from '../../theme';
const colors = themeColors.colors;

// Models
import {Error} from '../../models';

const StateCard: React.FC<Error> = ({text, color, icon, timeout}) => {
  const [modalVisible, setModalVisible] = useState<boolean>(false);

  const handleTimeout = useCallback(() => {
    setTimeout(() => {
      setModalVisible(false);
    }, timeout);
  }, [timeout]);

  useEffect(() => {
    let mounted = true;

    if (mounted) {
      setModalVisible(true);
    }
    if (timeout) {
      handleTimeout();
    }

    return () => {
      mounted = false;
      setModalVisible(false);
    };
  }, [timeout, handleTimeout]);

  return (
    <Modal animationType="slide" transparent={true} visible={modalVisible}>
      <View style={styles.centeredView}>
        <View style={styles.modalView}>
          <Icon name={icon} size={40} color={color} />
          <Text style={[styles.text, {color: color}]}>{text}</Text>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: colors.white,
    borderRadius: 20,
    padding: 35,
    justifyContent: 'space-between',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  text: {
    fontWeight: '700',
  },
});

export default StateCard;
