import React from 'react';
import {View, Text, Dimensions, StyleSheet, Image} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

// Dimensions
const {width, height} = Dimensions.get('screen');

// Models
import {Coin} from '../../models';

// Theme
import {themeColors} from '../../theme';
const colors = themeColors.colors;

type Props = {
  data: Coin;
};

const SingleCoinCard: React.FC<Props> = ({data}) => {
  return (
    <View style={styles.container}>
      {data && (
        <View style={styles.innerContainer}>
          <Image source={{uri: data.image}} style={styles.image} />
          <View style={styles.textContainer}>
            <Text style={[styles.text, styles.title]}>{data.name}</Text>
            <Text style={[styles.text, {color: colors.darkGrey}]}>
              {data.symbol}
            </Text>
          </View>
          <View style={styles.priceContainer}>
            <Text style={styles.text}>$ {data.current_price}</Text>
            <View style={styles.price}>
              <Icon
                name={
                  data.price_change_percentage_24h > 0
                    ? 'chevron-up'
                    : 'chevron-down'
                }
                color={
                  data.price_change_percentage_24h > 0
                    ? colors.success
                    : colors.error
                }
                size={16}
              />
              <Text
                style={[
                  styles.text,
                  data.price_change_percentage_24h > 0
                    ? {color: colors.success}
                    : {color: colors.error},
                ]}>
                % {data.price_change_percentage_24h.toFixed(2)}
              </Text>
            </View>
          </View>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
    height: height * 0.8,
  },
  innerContainer: {
    width: width * 0.8,
    backgroundColor: colors.white,
    borderRadius: 35,
    padding: 20,
    alignItems: 'center',
  },
  image: {
    width: 120,
    height: 120,
  },
  text: {
    fontFamily: 'Poppins-Regular',
  },
  textContainer: {
    padding: 20,
    alignItems: 'center',
  },
  title: {
    fontSize: 20,
  },
  priceContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: width * 0.5,
  },
  price: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export default SingleCoinCard;
