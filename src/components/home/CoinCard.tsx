import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {useNavigation} from '@react-navigation/native';

// Dimensions
const {width} = Dimensions.get('screen');

// Interfaces
import {Coin, Navigation} from '../../models';

// Theme
import {themeColors} from '../../theme';
import {StackNavigationProp} from '@react-navigation/stack';
const colors = themeColors.colors;

interface Props {
  data: Coin;
  navigation: Function;
}

type singleCoinScreenProp = StackNavigationProp<Navigation>;

const CoinCard: React.FC<Props> = React.memo(({data}) => {
  const navigation = useNavigation<singleCoinScreenProp>();
  return (
    <TouchableOpacity
      onPress={() => navigation.navigate('SingleCoin', {data: data})}>
      <View style={styles(width * 0.9).container}>
        <View
          style={[styles(width * 0.15).innerContainer, {alignItems: 'center'}]}>
          <Image source={{uri: data.image}} style={styles(width * 0.1).image} />
        </View>
        <View style={styles(width * 0.4).innerContainer}>
          <Text style={styles(undefined, colors.black).text}>{data.name}</Text>
          <Text style={styles(undefined, colors.darkGrey).text}>
            {data.symbol}
          </Text>
        </View>
        <View style={styles(width * 0.2).innerContainer}>
          <Text style={styles(undefined, colors.black).text}>
            $ {data.current_price}
          </Text>
          <View style={styles().percentageContainer}>
            <Icon
              name={
                data.price_change_percentage_24h > 0
                  ? 'chevron-up'
                  : 'chevron-down'
              }
              color={
                data.price_change_percentage_24h > 0
                  ? colors.success
                  : colors.error
              }
              size={16}
            />
            <Text
              style={[
                styles().text,
                data.price_change_percentage_24h > 0
                  ? {color: colors.success}
                  : {color: colors.error},
              ]}>
              % {data.price_change_percentage_24h.toFixed(2)}
            </Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
});

const styles = (widthSize?: number, textColor?: string) =>
  StyleSheet.create({
    container: {
      flexDirection: 'row',
      backgroundColor: colors.white,
      padding: 8,
      margin: 5,
      width: widthSize,
      borderRadius: 20,
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    innerContainer: {
      width: widthSize,
    },
    image: {
      width: widthSize,
      height: width * 0.1,
    },
    text: {
      fontFamily: 'Poppins-Regular',
      color: textColor,
    },
    percentageContainer: {
      flexDirection: 'row',
      alignItems: 'center',
    },
  });

export default CoinCard;
