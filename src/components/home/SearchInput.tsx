import React from 'react';
import {View, TextInput, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {Input} from '../../models';

// Theme
import {themeColors} from '../../theme';
const colors = themeColors.colors;

const SearchInput: React.FC<Input> = ({
  widthSize,
  icon,
  text,
  textColor,
  backgroundColor,
  handleChange,
}) => {
  return (
    <View
      style={[
        styles.container,
        {
          width: widthSize,
          backgroundColor: backgroundColor,
        },
      ]}>
      <Icon style={styles.icon} name={icon} size={25} />

      <TextInput
        placeholder={text}
        placeholderTextColor={textColor}
        style={{width: widthSize * 0.8}}
        onChangeText={handleChange}
        clearButtonMode="always"
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 50,
    borderRadius: 20,
    alignContent: 'center',
    padding: 12,
    flexDirection: 'row',
  },
  icon: {
    paddingRight: 8,
  },
  closeIcon: {
    color: colors.darkGrey,
    alignSelf: 'center',
    paddingRight: 20,
  },
});

export default SearchInput;
