/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect, useState} from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  RefreshControl,
  FlatList,
  Text,
} from 'react-native';
import {CoinCard, SearchInput, CoinListLoader, Error} from '../components';
import BASE_URL from '../../config/baseURL';

// Theme
import {themeColors} from '../theme';
import {SafeAreaView} from 'react-native-safe-area-context';
const colors = themeColors.colors;

// React Hooks
import {useQuery} from 'react-query';

// Models
import {Coin} from '../models';

// Dimensions
const {width} = Dimensions.get('screen');

interface Props {
  navigation: Function;
}

const CoinList: React.FC<Props> = ({navigation}) => {
  const {data, isLoading, refetch} = useQuery('getList', async () => {
    const res = await fetch(
      `${BASE_URL}/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=100&page=1&sparkline=false`,
    );
    if (!res.ok) {
      setError('Something went wrong');
    }
    return res.json();
  });

  const [list, setList] = useState<Coin[]>([]);
  const [search, setSearch] = useState<string>('');
  const [error, setError] = useState<string>();

  useEffect(() => {
    let mounted = true;
    if (mounted && data) {
      setList(data);
    }
    return () => {
      mounted = false;
    };
  }, [data]);

  // Search Method

  useEffect(() => {
    if (!error) {
      setList(
        data?.filter((item: Coin) =>
          item.name.toLowerCase().includes(search.toLowerCase()),
        ),
      );
    }
  }, [search, data]);

  return (
    <SafeAreaView>
      <View style={styles.container}>
        <View style={styles.inputContainer}>
          {/* INPUT */}
          <SearchInput
            icon={'search-outline'}
            widthSize={width * 0.9}
            textColor={colors.darkGrey}
            backgroundColor={colors.white}
            text={'Search a coin'}
            handleChange={(e: string) => setSearch(e)}
          />
        </View>

        {/* LOADER */}
        {isLoading && <CoinListLoader />}

        {/* LIST */}
        {list && list.length > 0 && (
          <FlatList
            initialNumToRender={5}
            refreshControl={
              <RefreshControl onRefresh={refetch} refreshing={isLoading} />
            }
            bounces={true}
            contentContainerStyle={{paddingTop: 20}}
            data={list}
            renderItem={item => (
              <CoinCard navigation={navigation} data={item.item} />
            )}
            keyExtractor={item => item.id}
          />
        )}

        {/* EMPTY LIST */}
        {list && list.length === 0 && (
          <View>
            <Text style={styles.text}>
              There are no matches meeting your criteria.
            </Text>
          </View>
        )}
      </View>

      {/* ERROR ALERT */}
      {error && (
        <Error
          color={colors.error}
          text={'Something went wrong'}
          icon={'alert-outline'}
          timeout={4000}
        />
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    paddingTop: 20,
  },
  inputContainer: {
    paddingBottom: 10,
  },
  skeletonItem: {
    margin: 5,
    width: width * 0.9,
    height: 60,
    borderRadius: 20,
    alignSelf: 'center',
  },
  text: {
    fontFamily: 'Poppins-Regular',
  },
});

export default CoinList;
