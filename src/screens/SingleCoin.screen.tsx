import React from 'react';
import {View} from 'react-native';

// Components
import {SingleCoinCard, SingleCoinLoader} from '../components';

interface Props {
  route: any;
}

const SingleCoin: React.FC<Props> = ({route}) => {
  return (
    <View>
      {route.params.data && <SingleCoinCard data={route.params.data} />}
      {!route.params.data && <SingleCoinLoader />}
    </View>
  );
};

export default SingleCoin;
