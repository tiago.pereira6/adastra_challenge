export default {
  colors: {
    primary: '#4564F6',
    secondary: '#60CEF2',
    error: '#FF3333',
    success: '#4BB543',
    black: '#273036',
    grey: '#f3f4f5',
    darkGrey: '#C1C1C1',
    white: '#FFFFFF',
  },
};
