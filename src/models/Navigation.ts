import {Coin} from './Coin';

export type Navigation = {
  CoinList: undefined;
  SingleCoin: {data: Coin};
};
