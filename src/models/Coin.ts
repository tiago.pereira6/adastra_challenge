export interface Coin {
  price_change_percentage_24h: number;
  id: string;
  image: string;
  current_price: number;
  name: string;
  symbol: string;
}
