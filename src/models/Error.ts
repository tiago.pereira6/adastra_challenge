export interface Error {
  text: string;
  color: string;
  icon: string;
  timeout?: number;
}
