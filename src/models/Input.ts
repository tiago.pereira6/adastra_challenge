export interface Input {
  widthSize: number;
  icon: string;
  text: string;
  textColor: string;
  backgroundColor?: string;
  handleChange?: (text: string) => void;
}
