# Adastra_challenge

## Branches

**- Production:** main

**- Dev:** master

## Install Project

- In terminal go to project folder and install dependencies and pods

```
yarn install
sudo gem install cocoapods
cd ios && pod install
```

## Run on iOS

`npx react-native run-ios`


## Testing

`yarn test`
