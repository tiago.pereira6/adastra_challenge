/**
 * @format
 */

import 'react-native';
import React from 'react';
import renderer from 'react-test-renderer';
import {SingleCoinCard} from '../src/components';

const data = {
  price_change_percentage_24h: 200,
  id: 'btc',
  image:
    'https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/BTC_Logo.svg/1200px-BTC_Logo.svg.png',
  current_price: 40000,
  name: 'bitcoin',
  symbol: 'btc',
};

test('renders correctly', () => {
  const tree = renderer.create(<SingleCoinCard data={data} />).toJSON();
  expect(tree).toMatchSnapshot();
});
